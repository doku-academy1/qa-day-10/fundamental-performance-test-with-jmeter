/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.86301369863014, "KoPercent": 0.136986301369863};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.4712328767123288, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.49, 500, 1500, "login page-0"], "isController": false}, {"data": [0.52, 500, 1500, "login page-1"], "isController": false}, {"data": [0.18, 500, 1500, "open root page"], "isController": false}, {"data": [0.5, 500, 1500, "open root page-1"], "isController": false}, {"data": [0.5125, 500, 1500, "open root page-0"], "isController": false}, {"data": [0.52, 500, 1500, "delete item"], "isController": false}, {"data": [0.5, 500, 1500, "create item"], "isController": false}, {"data": [0.53, 500, 1500, "get all suppliers"], "isController": false}, {"data": [0.55, 500, 1500, "update supplier"], "isController": false}, {"data": [0.52, 500, 1500, "create supplier"], "isController": false}, {"data": [0.5, 500, 1500, "get all items"], "isController": false}, {"data": [0.52, 500, 1500, "get reports spesific customer"], "isController": false}, {"data": [0.54, 500, 1500, "delete supplier"], "isController": false}, {"data": [0.18, 500, 1500, "login page"], "isController": false}, {"data": [0.52, 500, 1500, "update item"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 730, 1, 0.136986301369863, 946.3890410958901, 240, 2446, 870.0, 1475.8, 1766.4999999999993, 2179.1099999999988, 12.381065450043248, 32.01811896847069, 13.118559699419956], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["login page-0", 50, 0, 0.0, 872.44, 459, 1656, 863.5, 1211.6999999999998, 1528.5499999999995, 1656.0, 0.9555661729574773, 0.7521350931677018, 0.7810633659818442], "isController": false}, {"data": ["login page-1", 50, 0, 0.0, 838.6599999999999, 240, 1194, 849.0, 1155.1, 1178.1499999999999, 1194.0, 0.9524172349422835, 3.3212574764752945, 0.6687382733627948], "isController": false}, {"data": ["open root page", 50, 0, 0.0, 1540.8399999999997, 661, 2273, 1560.0, 1914.7, 2095.0, 2273.0, 0.9412473409762617, 3.870070805331225, 0.9357322198377289], "isController": false}, {"data": ["open root page-1", 40, 0, 0.0, 821.4250000000002, 633, 1116, 816.0, 974.4, 999.1999999999999, 1116.0, 0.9604302727621975, 3.3492660962111023, 0.5449316293699576], "isController": false}, {"data": ["open root page-0", 40, 0, 0.0, 826.9249999999997, 493, 1456, 798.5, 1042.3, 1181.7999999999997, 1456.0, 0.9597389510053266, 0.8969435313594702, 0.5407904050098373], "isController": false}, {"data": ["delete item", 50, 0, 0.0, 832.0400000000001, 368, 1029, 870.0, 947.0, 1004.65, 1029.0, 0.9789333542172449, 1.1366716388323284, 0.7331675468909077], "isController": false}, {"data": ["create item", 50, 1, 2.0, 862.22, 386, 1205, 894.0, 1022.6999999999999, 1190.25, 1205.0, 0.9537070593396533, 1.1395495462738665, 2.469188556230568], "isController": false}, {"data": ["get all suppliers", 50, 0, 0.0, 802.84, 272, 1144, 851.0, 934.6, 970.9499999999998, 1144.0, 0.9867382380802021, 4.0827836379065365, 0.6167113988001264], "isController": false}, {"data": ["update supplier", 50, 0, 0.0, 787.0799999999997, 287, 1133, 799.0, 1103.2, 1133.0, 1133.0, 0.9955201592832255, 1.169444530861125, 0.9614935913389746], "isController": false}, {"data": ["create supplier", 50, 0, 0.0, 779.7199999999999, 368, 1104, 810.0, 946.4, 1037.2999999999997, 1104.0, 0.9925755349982134, 1.1663344123456545, 0.9547723652082423], "isController": false}, {"data": ["get all items", 50, 0, 0.0, 859.82, 549, 1244, 843.5, 1097.1, 1207.7499999999998, 1244.0, 0.9495774380400721, 5.23187494065141, 0.5888492901908651], "isController": false}, {"data": ["get reports spesific customer", 50, 0, 0.0, 974.68, 320, 1635, 992.5, 1253.5, 1507.4499999999994, 1635.0, 1.0133149585554182, 4.931815778073891, 0.707539253288207], "isController": false}, {"data": ["delete supplier", 50, 0, 0.0, 768.8999999999996, 270, 1397, 780.5, 937.9, 1019.8999999999999, 1397.0, 1.0009008107296566, 1.1680434265839257, 0.7565402612351116], "isController": false}, {"data": ["login page", 50, 0, 0.0, 1712.2800000000002, 1105, 2446, 1730.5, 2233.6, 2374.5, 2446.0, 0.9364348054088475, 4.00260036240027, 1.4229419504064127], "isController": false}, {"data": ["update item", 50, 0, 0.0, 867.0800000000002, 389, 1191, 906.0, 1022.3, 1072.1499999999999, 1191.0, 0.9674361008455392, 1.1334496473695412, 2.483249750885204], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["500/Internal Server Error", 1, 100.0, 0.136986301369863], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 730, 1, "500/Internal Server Error", 1, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["create item", 50, 1, "500/Internal Server Error", 1, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
